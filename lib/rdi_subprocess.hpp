#ifndef RDI_SUBPROCESS_HPP
#define RDI_SUBPROCESS_HPP


#include <stddef.h>


#define COMMAND_DOES_NOT_EXIST 8273416

/// @brief A simple bytes array and its size :)
struct RDI_Buffer
{
    char* data;
    size_t size;
    size_t capacity;
};

typedef struct RDI_Buffer RDI_Buffer;

/// @brief for when you need to pass a buffer that is constant
///        like a C string literal, or data that is owned by some one else
///        like mystdvector.data()
struct RDI_Const_Buffer
{
    const char* data;
    size_t size;
};


/// \mainpage rdi_subprocess RDI Subprocess
/// A little lib we use to do synchronous calls to subprocesses. :)
///
/// - \ref rdi_subprocess_io()
/// - \ref rdi_subprocess_io_l()
/// - \ref rdi_subprocess_io_s()
/// - \ref rdi_subprocess_o()
/// - \ref rdi_subprocess_o_l()
/// - \ref rdi_subprocess_o_s()
/// - \ref rdi_subprocess_i()
/// - \ref rdi_subprocess_i_l()
/// - \ref rdi_subprocess_i_s()

/// \file rdi_subprocess.hpp
/// Okay it's an hpp file for a c library because all of our internal usage is
/// C++. I don't know why I implemented this in C. But I like C :)

#ifndef __cplusplus
#define nullptr NULL

#include <stdbool.h>
#else
extern "C"
{
#endif

typedef struct RDI_Const_Buffer RDI_Const_Buffer;

/// @brief Two way communication with a subprocess.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input_buffer Command input
/// @param command and arguments, null terminated.
/// \warning if you forget the null in the end, bad things will happen.
/// @returns command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_io(int* status, RDI_Const_Buffer input_buffer, const char* command, ...);

/// @brief Two way communication with a subprocess.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input_buffer Command input
/// @param argv command and arguments in a null terminated array.
/// \warning if you forget the null in the end, bad things will happen.
/// @returns command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_io_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[]);

/// @brief Two way communication with a subprocess.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input_buffer Command input
/// @param cmd_line The command line as you would write it in a shell.
///        No crazy shit allowed like redirection or piping. Just the command
///        and its arguments.
/// @returns command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_io_s(int* status, RDI_Const_Buffer input_buffer, const char* cmd_line);

/// @brief Command that takes no input from the stdin. But we will capture its
/// stdout and return it.
/// @param status the return status of the program. Will be set to
/// COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param command and arguments, null terminated.
/// \warning if you forget the null in the end, bad things will happen.
/// @returns Command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_o(int* status, const char* command, ...);

/// @brief Command that takes no input from the stdin. But we will capture its
///        stdout and return it.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param cmd_line The command line as you would write it in a shell.
///        No crazy shit allowed like redirection or piping. Just the command
///        and its arguments.
/// @returns Command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_o_s(int* status, const char* cmd_line);

/// @brief Command that takes no input from the stdin. But we will capture its
///        stdout and return it.
///        Exactly like rdi_subprocess_o but takes a null-terminalted array
///        instead of ellipsis arguments.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param argv command and arguments in a null terminated array.
/// \warning if you forget the null in the end, bad things will happen.
/// @returns Command output
/// Freeing the returned buffer (using rdi_buffer_free()) is your responsibility.
RDI_Buffer
rdi_subprocess_o_l(int* status, const char* argv[]);

/// @brief Provide input to the command's stdin. Output is not captured.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input input buffer
/// @param command the name of command
/// @param ... the arguments, null terminated.
/// \warning if you forget the null in the end, bad things will happen.
void
rdi_subprocess_i(int* status, RDI_Const_Buffer input, const char* command, ...);

/// @brief Provide input to the command's stdin. Output is not captured.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input input buffer
/// @param command the name of command
/// @param argv command and arguments in a null terminated array.
/// \warning if you forget the null in the end, bad things will happen.
void
rdi_subprocess_i_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[]);

/// @brief Provide input to the command's stdin. Output is not captured.
/// @param status the return status of the program. Will be set to
///		   COMMAND_DOES_NOT_EXIST if the command doesn't exist.
/// @param input input buffer
/// @param cmd_line The command line as you would write it in a shell.
///        No crazy shit allowed like redirection or piping. Just the command
///        and its arguments.
void
rdi_subprocess_i_s(int* status, RDI_Const_Buffer input, const char* cmd_line);

/// @brief will free the buffer b if it's not pointing to null
void
rdi_buffer_free(RDI_Buffer b);

/// @brief will create a buffer of size `size` and return it.
RDI_Buffer
rdi_buffer_new(size_t size);

/// @brief will create a buffer in its initial state.
/// points to null and size = 0
RDI_Buffer
rdi_buffer_init(void);

/// @brief expands/shrinks the buffer to new_size
/// can take a buffer in its initial state, look at \ref rdi_buffer_init()
/// changes buffer size 
/// Careful, the pointer in the buffer may change.
RDI_Buffer rdi_buffer_resize(RDI_Buffer b, size_t new_size);

/// @brief reserves memory to be used later when calling rdi_buffer_resize 
/// changes buffer capacity 
/// Careful, the pointer in the buffer may change.
RDI_Buffer rdi_buffer_reserve(RDI_Buffer b, size_t capacity);

/// @brief a convenience function for those using this lib to deal with strings
/// RDI_Buffers have a size attribute and so the data buffer does not have a
/// null terminator by default.
/// Call this function to null terminate the buffer.
RDI_Buffer
rdi_buffer_null_terminate(RDI_Buffer b);

bool
command_exists(const char* command) ;


#ifdef __cplusplus
} // extern "C"

#include <string>
#include <vector>

namespace RDI
{

/// @brief Convenience function that executes command and get its
/// output in an std::string
/// @param command The command line as you would write it in a shell.
///        No crazy shit allowed like redirection or piping. Just the command
///        and its arguments.
inline std::string
execute_s(int& status, const std::string& command)
{
	RDI_Buffer output_b = rdi_subprocess_o_s(&status, command.c_str());

	std::string output(output_b.data, output_b.size);

    rdi_buffer_free(output_b);

    return output;
}

} // namespace RDI

#endif // __cplusplus

#endif // RDI_SUBPROCESS_HPP
