#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include <fcntl.h>
#include <stdarg.h>
#include <signal.h>
#include <errno.h>

#include "rdi_subprocess.hpp"

static size_t
min(size_t first, size_t second)
{
	if(first < second)
	{
		return first;
	}

	return second;
}

static void
write_to_fd(int fd, RDI_Const_Buffer b)
{
	// sigpipe happens when the child process needs only part of the
	// input to produce the desired output
	// so after taking the input it needs it closes the pipe and exits
	// leaving us (the parent process) with a broken pipe
	// So we ignore the signal and handle it locally
	signal(SIGPIPE, SIG_IGN);

	const size_t CHUNK_SIZE = 50;

	if(b.size <= CHUNK_SIZE)
	{
		write(fd, b.data, b.size);
		return;
	}

	size_t i;
	for(i = 0; i < b.size - CHUNK_SIZE; i += CHUNK_SIZE)
	{
		write(fd, b.data + i, CHUNK_SIZE);

		// don't worry errno is thread-safe
		if(errno == EPIPE)
		{
			close(fd);
			return;
		}
	}

	size_t last_bit = b.size - i;

	write(fd, b.data + i, last_bit);
}

struct Stream
{
	RDI_Buffer b;
	size_t read_bytes;
	ssize_t last_read;
};

typedef struct Stream Stream;

static Stream
stream_init()
{
	Stream s;
	s.b = rdi_buffer_init();
	s.read_bytes = 0;
	s.last_read = 0;
	return s;
}

static Stream
read_from_fd(int fd, Stream out, size_t max_read_size)
{
	size_t max_array_size = INT_MAX - 1;

	if (out.read_bytes + max_read_size >= max_array_size)
	{
		// If resize would fail, don't read more, return what we have.
		return out;
	}

	out.b = rdi_buffer_resize(out.b, out.read_bytes + max_read_size);
	out.last_read = read(fd, out.b.data + out.read_bytes, max_read_size);

	if (out.last_read > 0)
	{
		out.read_bytes += (size_t)out.last_read;
	}

	if(out.read_bytes > 0)
	{
		out.b = rdi_buffer_resize(out.b, out.read_bytes);
	}
	else if(out.read_bytes == 0)
	{
		rdi_buffer_free(out.b);
		out.b = rdi_buffer_init();
	}

	return out;
}

static RDI_Buffer
read_all_stdout(int r_fd)
{
	const size_t CHUNK_SIZE = 4096;
	Stream output = stream_init();

	do
	{
		output = read_from_fd(r_fd, output, CHUNK_SIZE);
	}
	while (output.last_read > 0);

	if(output.read_bytes > 0)
	{
		output.b = rdi_buffer_resize(output.b, output.read_bytes);
	}

	return output.b;
}

static void
fd_set_blocking(int fd, bool blocking)
{
	// Save the current flags
	int flags = fcntl(fd, F_GETFL, 0);

	if (blocking)
	{
		flags &= ~O_NONBLOCK;
	}
	else
	{
		flags |= O_NONBLOCK;
	}

	fcntl(fd, F_SETFL, flags);
}

static RDI_Buffer
rw_from_fd(int w_fd, int r_fd, RDI_Const_Buffer input)
{
	// sigpipe happens when the child process needs only part of the
	// input to produce the desired output
	// so after taking the input it needs it closes the pipe and exits
	// leaving us (the parent process) with a broken pipe
	// So we ignore the signal and handle it locally
	signal(SIGPIPE, SIG_IGN);

	const size_t CHUNK_SIZE = 4096;

	Stream output = stream_init();

	if(input.size <= CHUNK_SIZE)
	{
		write(w_fd, input.data, input.size);
		close(w_fd);

		RDI_Buffer output = read_all_stdout(r_fd);
		close(r_fd);
		return output;
	}

	fd_set_blocking(w_fd, false);
	fd_set_blocking(r_fd, false);

	size_t i;
	for(i = 0; i < input.size;)
	{
		size_t remaining_bytes = input.size - i;
		size_t bytes_to_write = min(CHUNK_SIZE, remaining_bytes);
		ssize_t bytes_written = write(w_fd, input.data + i, bytes_to_write);

		if(bytes_written > 0)
		{
			i += (size_t)bytes_written;
		}

		if(remaining_bytes == 0)
		{
			break;
		}

		output = read_from_fd(r_fd, output, CHUNK_SIZE);

		// don't worry errno is thread-safe
		if(bytes_written == -1 && errno == EPIPE)
		{
			goto return_output;
		}
	}

	close(w_fd);

	fd_set_blocking(w_fd, true);
	fd_set_blocking(r_fd, true);

	do
	{
		output = read_from_fd(r_fd, output, CHUNK_SIZE);
	}
	while (output.last_read > 0);

	close(r_fd);

	output.b = rdi_buffer_resize(output.b, output.read_bytes);

	return output.b;

	return_output:

	close(w_fd);
	close(r_fd);
	return output.b;
}

#include <fcntl.h>
#include <errno.h>

static int
is_within(int fd, int arr[2])
{
	for(int i = 0; i < 2; i++)
	{
		if(fd == arr[i])
		{
			return 1;
		}
	}

	return 0;
}

static int
fd_is_valid(int fd)
{
	return fcntl(fd, F_GETFD) != -1 || errno != EBADF;
}

static void
close_all_descriptors_except(int exceptions[2])
{
	const int max_fd = getdtablesize();

	for (int fd = 3; fd <= max_fd; fd++)
	{
		if(fd_is_valid(fd) && !is_within(fd, exceptions))
		{
			close(fd);
		}
	}
}

bool
command_exists(const char* command) 
{
	const char* cmd_1st = "which ";
	const char* cmd_last = " > /dev/null 2>&1";

	char final_cmd[512];

	strcpy(final_cmd, cmd_1st);
	strcat(final_cmd, command);
	strcat(final_cmd, cmd_last);

	bool exists = true;

	if(system(final_cmd))
	{
		exists = false;
	}

	return exists;
}

RDI_Buffer
rdi_subprocess_io_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[])
{
	if(!command_exists(argv[0]))
	{
		*status = COMMAND_DOES_NOT_EXIST;
		RDI_Buffer b = rdi_buffer_init();
		return b;
	}

	int main_to_sub[2];
	int sub_to_main[2];

	pipe(main_to_sub);
	pipe(sub_to_main);

	int pid = fork();

	if(pid == 0)
	{
		int exceptions[2] = {main_to_sub[0], sub_to_main[1]};
		close_all_descriptors_except(exceptions);
		dup2(main_to_sub[0], STDIN_FILENO);
		dup2(sub_to_main[1], STDOUT_FILENO);

		close(main_to_sub[0]);
		close(sub_to_main[1]);

		execvp(argv[0], argv);
		exit(1);
	}

	close(main_to_sub[0]);
	close(sub_to_main[1]);

	RDI_Buffer output =
			rw_from_fd(main_to_sub[1], sub_to_main[0], input_buffer);

	waitpid(pid, status, 0);

	return output;
}

RDI_Buffer
rdi_subprocess_io(int* status, RDI_Const_Buffer input_buffer, const char* command, ...)
{
	va_list ap;
	int n;

	va_start(ap, command);
	for(n = 1; va_arg(ap, char *); n++);
	va_end(ap);

	const char * argv[128];

	int i = 0;

	argv[i] = command;

	va_start(ap, command);
	for(i = 1; i < n; i++)
		argv[i] = va_arg(ap, char *);
	va_end(ap);

	argv[i] = nullptr;

	return rdi_subprocess_io_l(status, input_buffer, argv);
}

RDI_Buffer
rdi_subprocess_io_s(int* status, RDI_Const_Buffer input_buffer, const char* cmd_line)
{
	const char* argv[] = {"sh", "-c", cmd_line, nullptr};
	return rdi_subprocess_io_l(status, input_buffer, argv);
}

RDI_Buffer
rdi_subprocess_o_l(int* status, const char* argv[])
{
	if(!command_exists(argv[0]))
	{
		*status = COMMAND_DOES_NOT_EXIST;
		RDI_Buffer b = rdi_buffer_init();
		return b;
	}

	int sub_to_main[2];

	pipe(sub_to_main);

	int pid = fork();

	if(pid == 0)
	{
		int exceptions[2] = {sub_to_main[1], sub_to_main[1]};
		close_all_descriptors_except(exceptions);
		dup2(sub_to_main[1], STDOUT_FILENO);
		close(sub_to_main[1]);

		execvp(argv[0], argv);
		exit(1);
	}

	close(sub_to_main[1]);

	RDI_Buffer output = read_all_stdout(sub_to_main[0]);

	waitpid(pid, status, 0);

	close(sub_to_main[0]);

	return output;
}

RDI_Buffer
rdi_subprocess_o(int* status, const char* command, ...)
{
	va_list ap;
	int n;

	va_start(ap, command);
	for(n = 1; va_arg(ap, char *); n++);
	va_end(ap);

	const char *argv[128];

	int i = 0;

	argv[i] = command;

	va_start(ap, command);
	for(i = 1; i < n; i++)
		argv[i] = va_arg(ap, char *);
	va_end(ap);

	argv[i] = nullptr;

	return rdi_subprocess_o_l(status, argv);
}

RDI_Buffer
rdi_subprocess_o_s(int* status, const char* cmd_line)
{
	const char* argv[] = {"sh", "-c", cmd_line, nullptr};
	return rdi_subprocess_o_l(status, argv);
}

void
rdi_subprocess_i_l(int* status, RDI_Const_Buffer input_buffer, const char* argv[])
{
	if(!command_exists(argv[0]))
	{
		*status = COMMAND_DOES_NOT_EXIST;
		return;
	}

	int main_to_sub[2];

	pipe(main_to_sub);

	int pid = fork();

	if(pid == 0)
	{
		int exceptions[2] = {main_to_sub[0], main_to_sub[0]};
		close_all_descriptors_except(exceptions);
		dup2(main_to_sub[0], STDIN_FILENO);
		close(main_to_sub[0]);

		execvp(argv[0], argv);
		exit(1);
	}

	close(main_to_sub[0]);

	write_to_fd(main_to_sub[1], input_buffer);

	close(main_to_sub[1]);

	waitpid(pid, status, 0);
}

void
rdi_subprocess_i(int* status, RDI_Const_Buffer input_buffer, const char* command, ...)
{
	va_list ap;
	int n;

	va_start(ap, command);
	for(n = 1; va_arg(ap, char *); n++);
	va_end(ap);

	const char *argv[128];

	int i = 0;

	argv[i] = command;

	va_start(ap, command);
	for(i = 1; i < n; i++)
		argv[i] = va_arg(ap, char *);
	va_end(ap);

	argv[i] = nullptr;

	rdi_subprocess_i_l(status, input_buffer, argv);
}

void
rdi_subprocess_i_s(int* status, RDI_Const_Buffer input, const char* cmd_line)
{
	const char* argv[] = {"sh", "-c", cmd_line, nullptr};
	rdi_subprocess_i_l(status, input, argv);
}
