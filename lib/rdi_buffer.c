#include "rdi_subprocess.hpp"

#include <stdlib.h>
#include <string.h>
#include <stdio.h>

void
rdi_buffer_free(RDI_Buffer b)
{
	if(!b.data)
	{
		return;
	}

	free(b.data);
}

RDI_Buffer
rdi_buffer_init()
{
	RDI_Buffer b = {0};
	return b;
}

RDI_Buffer
rdi_buffer_reserve(RDI_Buffer b, size_t capacity)
{

	if ( capacity <= b.capacity )
	{
		return b;
	}

	char* new_data = realloc(b.data, capacity);

	if(new_data)
	{
		b.data = new_data;
		b.capacity = capacity;
		return b;
	}

	fprintf(stderr, "subprocess: not enough memory. exiting...\n");
	exit(1);
}


RDI_Buffer
rdi_buffer_new(size_t size)
{
	RDI_Buffer b;

	b.data = malloc(size);
	b.size = size;
	b.capacity = size;
	return b;
}

RDI_Buffer
rdi_buffer_resize(RDI_Buffer b, size_t new_size)
{
	if ( new_size <= b.capacity)
	{
		b.size = new_size;
		return b;
	}
	else
	{
		b.capacity = new_size;
		b.size = new_size;
	}

	if(new_size == 0)
	{
		rdi_buffer_free(b);
		return b;
	}

	char* new_data = realloc(b.data, new_size);

	if(new_data)
	{
		b.data = new_data;
		return b;
	}

	fprintf(stderr, "subprocess: not enough memory. exiting...\n");
	exit(1);
}

RDI_Buffer
rdi_buffer_null_terminate(RDI_Buffer b)
{
	b = rdi_buffer_resize(b, b.size + 1);
	b.data[b.size - 1] = '\0';
	return b;
}
