#include "catch.hpp"

#include "rdi_subprocess.hpp"

#include <rdi_wfile.hpp>

#include <iostream>
#include <string.h>

#ifdef __linux__
#include <sys/wait.h>
#endif

using namespace std;

static const char* tests_path = CODE_LOCATION "tests/";

TEST_CASE("spaces in arguments")
{
	std::string input(tests_path);
	input += "this file has spaces.wav";
	int status;

	RDI_Buffer obuf =
    rdi_subprocess_o(&status,
                     "ffmpeg", "-i", input.c_str(), "-f", "wav", "pipe:1",
                     nullptr);
	CHECK(status == 0);
	rdi_buffer_free(obuf);
}

TEST_CASE("rdi_subprocess_i")
{
	const char* thing =
			"Hello this is a sort of long text so that we can test how "
			"well this works. It should go with cat and be printed.\n";

	RDI_Const_Buffer in{
		thing,
		strlen(thing)
	};

	int status;
	rdi_subprocess_i(&status, in, "cat", nullptr);
	CHECK(status == 0);
}

TEST_CASE("rdi_subprocess_o")
{
	const char* expected =
			"Hello this is a sort of long text so that we can test how "
			"well this works. It should go with echo and be printed.\n";

	int status;
	RDI_Buffer out =
		rdi_subprocess_o(&status, "echo",
						 "Hello this is a sort of long text so that we can test how "
						 "well this works. It should go with echo and be printed.",
						 nullptr);
	cout << status << endl;

	out = rdi_buffer_null_terminate(out);
	cout << "-----------" << out.data;

	CHECK( strcmp(out.data, expected) == 0 );

	rdi_buffer_free(out);
}

TEST_CASE("rdi_subprocess_o_l")
{
	const char* expected =
			"Hello this is a sort of long text so that we can test how "
			"well this works. It should go with echo and be printed.\n";

	const char* argv[3] = {
		"echo",
		 "Hello this is a sort of long text so that we can test how "
		 "well this works. It should go with echo and be printed.",
		 nullptr
	};

	int status;
	RDI_Buffer out = rdi_subprocess_o_l(&status, argv);

	cout << status << endl;

	out = rdi_buffer_null_terminate(out);
	cout << "-----------" << out.data;

	CHECK( strcmp(out.data, expected) == 0 );

	rdi_buffer_free(out);
}

TEST_CASE("Let's sing a little song (ffmpeg test)")
{
	string input_path = string(tests_path) + "شسليستابتشبلاسستابسباسابستبنتسنبتاسبتسنباتباصهاي.wav";

	int status;
	RDI_Buffer output1 =
		rdi_subprocess_o(&status, "ffmpeg", "-hide_banner", "-loglevel", "warning", "-i",
						 input_path.c_str(), "-acodec",
						 "pcm_s16le", "-ac", "1", "-ar", "16000", "-f", "wav",
						 "pipe:1", "-y", nullptr);
	cout << status << endl;

	RDI_Const_Buffer c_output1{
		output1.data,
		output1.size
	};

	RDI_Buffer output2 =
		rdi_subprocess_io(&status, c_output1, "ffmpeg", "-hide_banner", "-loglevel",
						  "warning", "-i", "pipe:0", "-acodec", "pcm_s16le",
						  "-ac", "1", "-ar", "16000", "-f", "wav", "pipe:1",
						 "-y", nullptr);
	cout << status << endl;

	REQUIRE(output1.size == output2.size);

	CHECK( strncmp(output1.data, output2.data, output1.size) == 0 );

	rdi_buffer_free(output1);
	rdi_buffer_free(output2);
}

static vector<char>
split_wave(std::vector<char>& wave_bytes, float start, float end)
{
	int status;

	RDI_Const_Buffer in;
	in.data = wave_bytes.data();
	in.size = wave_bytes.size();

	RDI_Buffer out =
			rdi_subprocess_io(&status,
							  in,
							  "ffmpeg",
							  "-hide_banner",
							  "-i",
							  "pipe:0",
							  "-acodec",
							  "copy",
							  "-ss",
							  to_string(start).c_str(),
							  "-to",
							  to_string(end).c_str(),
							  "-f",
							  "wav",
							  "pipe:1",
							  nullptr);

	vector<char> output(out.data, out.data+out.size);

	rdi_buffer_free(out);

	return output;
}

TEST_CASE("When the child closes the pipe before you write all the data")
{
	pair< vector<char>, int> wave_bytes = RDI::read_binary_file(string(tests_path) + "10_19_20.wav");
	pair< vector<char>, int> expected = RDI::read_binary_file(string(tests_path) + "10_19_20_cut.wav");
	vector<char> actual = split_wave(wave_bytes.first, 0.0f, 4.5f);

	REQUIRE(actual.size() == expected.first.size());

	for(size_t i = 0; i < actual.size(); i++)
	{
		CHECK(actual[i] == expected.first[i]);
	}
}

TEST_CASE("command doesn't exist")
{
	int status;
	RDI_Const_Buffer b{};

	rdi_subprocess_i(&status, b,
					 "jklsdfajklsdfajklsdfajkl", "option", "lol", nullptr);

	CHECK(status == COMMAND_DOES_NOT_EXIST);
}

TEST_CASE("multi-threading: rdi_subprocess_io openmp")
{

#pragma omp parallel for
	for(int i = 0; i < 100; i++)
	{
		const char* thing =
				"Hello this is a sort of long text so that we can test how "
				"well this works. It should go with cat and be printed.";

		RDI_Const_Buffer in;
		in.data = thing;
		in.size = strlen(thing);

		int status;
		RDI_Buffer out = rdi_subprocess_io(&status, in, "cat", nullptr);

		out = rdi_buffer_null_terminate(out);

		CHECK( strcmp(out.data, thing) == 0 );

		rdi_buffer_free(out);
	}
}

TEST_CASE("multi-threading: rdi_subprocess_i openmp")
{
#pragma omp parallel for
	for(int i = 0; i < 100; i++)
	{
		const char* thing =
				"Hello this is a sort of long text so that we can test how "
				"well this works. It should go with cat and be printed.\n";

		RDI_Const_Buffer in;
		in.data = thing;
		in.size = strlen(thing);

		int status;
		rdi_subprocess_i(&status, in, "cat", nullptr);
		CHECK(status == 0);
	}
}

TEST_CASE("multi-threading: rdi_subprocess_o openmp")
{
#pragma omp parallel for
	for(int i = 0; i < 100; i++)
	{
		const char* expected =
				"Hello this is a sort of long text so that we can test how "
				"well this works. It should go with echo and be printed.\n";

		int status;
		RDI_Buffer out =
			rdi_subprocess_o(&status, "echo",
							 "Hello this is a sort of long text so that we can test how "
							 "well this works. It should go with echo and be printed.",
							 nullptr);

		out = rdi_buffer_null_terminate(out);

		CHECK( strcmp(out.data, expected) == 0 );

		rdi_buffer_free(out);
	}
}

TEST_CASE("multi-threading: rdi_subprocess_o_l openmp")
{
#pragma omp parallel for
	for(int i = 0; i < 100; i++)
	{
		const char* expected =
				"Hello this is a sort of long text so that we can test how "
				"well this works. It should go with echo and be printed.\n";

		const char* argv[3] = {
			"echo",
			 "Hello this is a sort of long text so that we can test how "
			 "well this works. It should go with echo and be printed.",
			 nullptr
		};

		int status;
		RDI_Buffer out = rdi_subprocess_o_l(&status, argv);

		out = rdi_buffer_null_terminate(out);

		CHECK( strcmp(out.data, expected) == 0 );

		rdi_buffer_free(out);
	}
}

TEST_CASE("execute_s")
{
	const char* expected =
			"Hello this is a sort of long text so that we can test how "
			"well this works. It should go with echo and be printed.\n";

	int status;
	string out =
		RDI::execute_s(status,
					   "echo Hello this is a sort of long text so that we can "
					   "test how well this works. "
					   "It should go with echo and be printed.");

	cout << status << endl;

	cout << out << endl;

	CHECK( strcmp(out.c_str(), expected) == 0 );
}

TEST_CASE("command exists")
{
	bool ok;
	#ifdef __linux__ 
		ok = command_exists("grep");
	#elif _WIN32
		ok = command_exists("XCOPY");
	#endif
	CHECK( ok == true );


	#ifdef __linux__ 
		ok = command_exists("zzzzzzz");
	#elif _WIN32
		ok = command_exists("zzzzzzz");
	#endif
	CHECK( ok == false );

}
